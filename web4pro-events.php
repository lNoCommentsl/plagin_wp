<?php
/*
Plugin Name: web4pro-events
Plugin URI: http://wordpress.loc
Description: показывает какие события скоро будут
Version:Версии плагина: 1.0
Author: NoComments
Author URI: http://wordpress.loc
License: GPLv2
*/

require_once dirname( __FILE__ ) . '/web4pro_events_widget.php';

add_action( 'init', 'register_post_events' );
function register_post_events() {
	register_post_type( 'post_events', array(
		'labels'               => array(
			'name'               => 'События', // основное название для типа записи
			'singular_name'      => 'Событие', // название для одной записи этого типа
			'add_new'            => 'Добавить новое событие', // для добавления новой записи
			'add_new_item'       => 'Добавление событие', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование', // для редактирования типа записи
			'new_item'           => 'Новое сбытие', // текст новой записи
			'view_item'          => 'Смотреть', // для просмотра записи этого типа.
			'search_items'       => 'Искать событие', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
		),
		'public'               => true,
		'menu_position'        => 15,
		'supports'             => array( 'title', 'editor', 'comments' ),
		'capability_type'      => 'post',
		'taxonomies'           => array( 'status' ),
		'menu_icon'            => plugins_url( 'images/imgonline-com-ua-Resize-iUYfA8cHJYF614k.png', __FILE__ ),
		'has_archive'          => true,
		'rewrite'              => array( 'slug' => 'post-events' ),
		'register_meta_box_cb' => 'add_post_events_metaboxes'
	) );
}

add_action( 'add_meta_boxes', 'add_post_events_metaboxes' );


function add_post_events_metaboxes() {
	add_meta_box( 'post_events', 'Событие ', 'events', 'post_events', 'side' );
}

function events() {
	global $post;

	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	     wp_create_nonce( plugin_basename( __FILE__ ) ) . '" />';

	$status = get_post_meta( $post->ID, '_status', true );
	$date   = get_post_meta( $post->ID, '_date', true );


	echo '<p>Cтатус:</p>'; ?>
    <label for="_status">
    <select name="_status" id ="_status">
        <option  <?php if($status == 'Open event') echo 'selected';?>>Open event</option>
        <option  <?php if($status == 'Close event') echo 'selected';?>>Close event</option>
    </select><?php
	echo '<p>Дата:</p>';
	echo '<input type="date" name="_date" value="' . $date . '"   />';
}

function save_events_meta( $post_id, $post ) {
	if ( $post->post_type == 'post_events' ) {
		if ( ! wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename( __FILE__ ) ) ) {
			return $post->ID;
		}

		if ( ! current_user_can( 'edit_post', $post->ID ) ) {
			return $post->ID;
		}

		$status = trim( strip_tags( $_POST['_status'] ) );
		$date   = trim( strip_tags( $_POST['_date'] ) );



		if (! empty( $date ) && ! empty($status)) {
			$events_meta['_status'] = $status;
			$events_meta['_date']   = $date;
		}


		foreach ( $events_meta as $key => $value ) {
			if ( $post->post_type == 'revision' ) {
				return;
			}
			$value = implode( ',', (array) $value );
			if ( get_post_meta( $post->ID, $key, false ) ) {
				update_post_meta( $post->ID, $key, $value );
			} else {
				add_post_meta( $post->ID, $key, $value );
			}
			if ( ! $value ) {
				delete_post_meta( $post->ID, $key );
			}

		}
	}
}

add_action( 'save_post', 'save_events_meta', 1, 2 );

add_action( 'init', 'create_event_taxonomies', 0 );

function create_event_taxonomies() {

	$labels = array(
		'name'              => 'Вид события',
		'singular_name'     => 'События',
		'search_items'      => 'Поиск событий',
		'all_items'         => 'Все события',
		'parent_item_colon' => 'Родительсоке событие:',
		'edit_item'         => 'Изменить событие',
		'update_item'       => 'Обновить событие',
		'add_new_item'      => 'Добавить новое событие ',
		'new_item_name'     => 'Добавить имя события',
		'menu_name'         => 'Вид события',
	);

	// Добавляем древовидную таксономию 'genre' (как категории)
	register_taxonomy( 'Мои таксономии', array( 'post_events' ), array(
		'hierarchical' => true,
		'labels'       => $labels,
		'show_ui'      => true,
		'query_var'    => true,
		'rewrite'      => array( 'slug' => '' ),
	) );
}

function upcoming_events( $atts ) {
	extract( shortcode_atts( array(
		'count'   => '5',
		'_status' => 'Открытое событие'
	), $atts ) );

	$args = array(
		'post_type'      => 'post_events',
		'posts_per_page' => $atts['count'],
		'meta_query'     => array(
			'relation' => 'AND',
			array(
				'key'     => '_status',
				'value'   => $atts['_status'],
				'compare' => '='
			),
			array(
				'key'     => '_date',
				'value'   => date( 'Y-m-d' ),
				'compare' => '>='
			)
		)
	);

	$my_posts = new WP_Query( $args );

	if ( $my_posts->have_posts() ) {

		while ( $my_posts->have_posts() ) {
			$my_posts->the_post();
			$post_id = $my_posts->post->ID;
			echo '<h2 style="margin-bottom:5px">Событие: ' . esc_html( get_the_title() ) . '</h2>';
			echo '<strong>' . 'Дата события: ' . '</strong>' . get_post_meta( $post_id, '_date', true ) . '</p> </br>';
		}
	} else {
		echo '<h2> По вашему запросу событий не найдено </h2>';
	}

	wp_reset_postdata();

}

add_shortcode( 'events', 'upcoming_events' );
