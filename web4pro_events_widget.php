<?php

class EventWidget extends WP_Widget
{
    public function __construct() {
        parent::__construct("event_widget", "Event Widget",
            array("description" => "A simple widget to show what events soon coming"));
    }

    public function form($instance) {
        $status = "";
        $count = "";
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $status = $instance["status"];
            $count = $instance["count"];
        }

        $countId = $this->get_field_id("count");
        $countName = $this->get_field_name("count");

        echo '<label for="'.$countId.'">Количество событий</label><br>';
        echo '<input id="'.$countId.'" type="text" name="'.$countName.'" value="' .$count . '"><br>';

        $statusId = $this->get_field_id("status");
        $statusName = $this->get_field_name("status");
        echo '<label for="'.$statusId.'">Статус события</label><br>';
	    ?>
	    
	    <select id ="<?php echo $statusId?> " name="<?php echo $statusName?>" >
            <option value="Open event" <?php if($status == 'Open event') echo 'selected';?>>Open event</option>
	        <option value="Close event" <?php if($status == 'Close event') echo 'selected';?>>Close event</option>
	    </select><?php

    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["status"] = htmlentities($newInstance["status"]);
        $values["count"] = htmlentities($newInstance["count"]);
        return $values;
    }
    public function widget($args, $instance) {

    	$status = $instance["status"];
        $count = $instance["count"];
        $arg =  array(
            'post_type' => 'post_events',
            'posts_per_page' => $count,
            'meta_query' => array(
                'relation' => 'AND',
	            array(
		            'key' => '_status',
		            'value' => $status,
		            'compare'=>'='),
                array(
                    'key' => '_date',
                    'value' => date('Y-m-d'),
                    'compare'=>'>='
                )

            )
        );

        $my_posts = new WP_Query($arg);

        if($my_posts->have_posts()) {

            while($my_posts->have_posts()){ $my_posts->the_post();
                $post_id = $my_posts->post->ID;
                echo '<h2 style="margin-bottom:5px">Событие: '.esc_html( get_the_title() ).'</h2>';
                echo '<strong>'.'Дата события: '.'</strong>'.   get_post_meta($post_id, '_date', true ).'</p> </br>';
            }
        }else{
            echo '<h2> По вашему запросу событий не найдено </h2>';
        }

        wp_reset_postdata();
    }
}

add_action("widgets_init", function () {
    register_widget("EventWidget");
});